import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const Film = props => (
    <a href="">
        <div className="film">
            <h1>{props.name}</h1>
            <p>Year: {props.year}</p>
            <img src={props.url} alt=""/>
        </div>
    </a>
);

const App = props => (
    <div className="app">
        <Film name="Justice league" year="2017" url="http://www.rabstol.net/uploads/gallery/main/649/rabstol_net_justice_league_08.jpg" />
        <Film name="Bright" year="2017" url="https://media.vanityfair.com/photos/5a3ae23b55764405e406a051/16:9/pass/Bright-Netflix-Review.jpg?mbid=social_retweet"/>
        <Film name="Blade runner" year="2017" url="http://www.chicagonow.com/hammervision/files/2017/10/blade-runner-2049-main-1.jpg"/>
    </div>
);


export default App;
